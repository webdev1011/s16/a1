let numberA = null
let numberB = null
let operation = null

//to retrieve an element from the webpage, we can use querySelector
let inputDisplay = document.querySelector('#txt-input-display');
//The documents refers to the whole web page and querySelector is use to select a specific object
//The querySelector function takes a string input that is formatted like CSS selector
//This allows us to get a specific element such as CSS selectors

//document.getElementById
//document.getElementByClassName
//document.getElementByTagname

let btnNumbers = document.querySelectorAll('.btn-numbers');
//btnAdd
let btnAdd = document.querySelector('#btn-add');
//btnSubtract
let btnSubtract = document.querySelector('#btn-subtract');
//btnMultiply
let btnMultiply = document.querySelector('#btn-multiply');
//btnDIvide
let btnDivide = document.querySelector('#btn-divide');
//btnEqual
let btnEqual = document.querySelector('#btn-equal');
//btnDecimal
let btnDecimal = document.querySelector('#btn-decimal');
//btnClearAll
let btnClearAll = document.querySelector('#btn-clear-all');
//btnBackspace
let btnBackspace = document.querySelector('#btn-backspace');

btnNumbers.forEach(function(btnNumber){
		btnNumber.onclick = () => {
			inputDisplay.value += btnNumber.textContent;
		}
})

btnAdd.onclick = () => {
	if (numberA == null) { // if numberA = none
		numberA = Number(inputDisplay.value); // display value = numberA
		operation = 'addition'; // add
		inputDisplay.value = null; // return display to none
	}
	else if (numberB == null){ // if numberB = none
		numberB = Number(inputDisplay.value); //
		numberA = numberA + numberB;
		operation = 'addition';
		numberB.value = null;
		inputDisplay.value = null;
	}
}

btnSubtract.onclick = () => {
	if (numberA == null) { 
		numberA = Number(inputDisplay.value); 
		operation = 'subtract'; 
		inputDisplay.value = null; 
	}
	else if (numberB == null){ 
		numberB = Number(inputDisplay.value); 
		numberA = numberA - numberB;
		operation = 'subtract';
		numberB.value = null;
		inputDisplay.value = null;
	}
}
btnMultiply.onclick = () => {
	if (numberA == null) { 
		numberA = Number(inputDisplay.value); 
		operation = 'multiply'; 
		inputDisplay.value = null; 
	}
	else if  (numberB == null){ 
		numberB = Number(inputDisplay.value); 
		numberA = numberA * numberB;
		operation = 'multiply';
		numberB.value = null;
		inputDisplay.value = null;
	}
}
btnDivide.onclick = () => {
	if (numberA == null) { 
		numberA = Number(inputDisplay.value); 
		operation = 'divide'; 
		inputDisplay.value = null; 
	}
	else if (numberB == null){ 
		numberB = Number(inputDisplay.value); 
		numberA = numberA / numberB;
		operation = 'divide';
		numberB.value = null;
		inputDisplay.value = null;
	}
}

btnEqual.onclick =  () =>{
	if (numberB == null && inputDisplay.value !== ''){
		numberB = inputDisplay.value ;
	}
	if (operation == 'addition'){
		inputDisplay.value = Number(numberA) + Number(numberB);
		NumberA = inputDisplay.value ;
	}
	if (operation == 'subtract'){
		inputDisplay.value = Number(numberA) - Number(numberB);
		NumberA = inputDisplay.value ;
	}
	if (operation == 'multiply'){
		inputDisplay.value = Number(numberA) * Number(numberB);
		NumberA = inputDisplay.value ;
	}
	if (operation == 'divide'){
		inputDisplay.value = Number(numberA) / Number(numberB);
		NumberA = inputDisplay.value ;
	}
	
}

btnClearAll.onclick = () => {
	numberA = null;
	NUmberB = null;
	operation = null;
	inputDisplay.value = null;
}
btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0, -1)
}

btnDecimal.onclick = () => {
	if (!inputDisplay.value.includes('.')) {
		inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
	}

}










